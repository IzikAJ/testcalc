registerRemoteCall = ()->
  $('form[data-remote] input[type="submit"]').off('click').on 'click', (e)->
    e.preventDefault()
    form = $(e.target).closest 'form[data-remote]'
    _loadResult form.attr('action'), form.attr('method'), form.serialize()

_loadResult = (action, method, data)->
  $.ajax(
      url:  action,
      type: method,
      data: data,
      dataType: 'html',
      success: (data)->
        _renderResult data
        _runCallback()
      except: (e)->
        _renderError e
    )
_renderResult = (data)->
  $('.result').html(data)
_runCallback = ()->
  registerCloseResultBtn()
  # TODO
_renderError = (e)->
  # TODO
  console.log "Error: #{e}"

registerCloseResultBtn = ()->
  $('.close-btn').off('click').on 'click', (e)->
    $('.result').empty()

$(document).ready ->
  registerRemoteCall()

window.registerRemoteCall = registerRemoteCall
window.registerCloseResultBtn = registerCloseResultBtn