require_relative 'credit'
class ClassicCredit < Credit
  # Погашение кредита дифференцированными платежами (основная сумма 
  # кредита выплачивается равными платежами, начисленные проценты 
  # с каждым следующим периодом уменьшаются, соответственно 
  # уменьшается и общая сумма платежа)

  def payment_list
    ans = []
    debt = @loan_amount
    repayment = @loan_amount / @loan_term

    @loan_term.times do |i|
      overpaid = @loan_amount * (@loan_term - i) * @yearly_fee / (1200 * @loan_term)
      payment = @loan_amount / @loan_term + overpaid
      debt = @loan_amount - repayment*(i+1)
      ans << {payment: payment, debt_payment: repayment,
        interest_payment: overpaid, debt: debt}
    end
    ans
  end

end
