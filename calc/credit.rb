require 'virtus'

class Credit
  include Virtus.model
  # Срок кредитования
  attribute :loan_term, Integer
  # Сумма кредита
  attribute :loan_amount, Float
  # Процентная ставка
  attribute :yearly_fee, Float
  # TODO
  # Единоразовая комиссия
  # Ежемесячная комиссия

  def errors
    errors = {}
    wrong = attributes.select {|k, v| (v.class == String) || !(v > 0)}
    wrong.each do |k, v|
      errors[k] = "The field must be a number and greater than 0"
    end
    errors
  end

  def payment_result
    begin
      list = payment_list
      payment = list.inject(0) { |sum, i| sum + i[:payment] }
      overpaid = payment - @loan_amount
      {
        payment_list: list,
        payment:  payment,
        overpaid: overpaid,
      }
    rescue NameError => e
      puts e.inspect
      raise "Payment list method not implemented."
    end
  end
end

