require 'enum_field'
require_relative 'classic_credit'
require_relative 'annuity_credit'

class CreditType
  def initialize(params)
    if params
      @name = params[:name]
      @type = params[:type]
    end
  end
  attr_reader :name
  attr_reader :type

  def build params
    @type.new(params)
  end

  define_enum do |b|
    b.member :annuity, object: CreditType.new(name: 'Аннуитентный', type: AnnuityCredit)
    b.member :classic, object: CreditType.new(name: 'Классический', type: ClassicCredit)
  end
end
