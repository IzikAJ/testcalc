require "sinatra/base"
require "sinatra/assetpack"
require "slim"
require_relative "calc/credit_type"

class App < Sinatra::Base
  set :root, File.dirname(__FILE__)
  register Sinatra::AssetPack

  assets do
    js_compression  :jsmin    # :jsmin | :yui | :closure | :uglify
    css_compression :simple   # :simple | :sass | :yui | :sqwish

    serve '/assets/js',    from: 'assets/javascripts'
    serve '/assets/css',   from: 'assets/stylesheets'
    serve '/assets/img',   from: 'assets/images'
    serve '/assets/fonts', from: 'assets/fonts' # fonts for bootstrap

    js :application, [
      '/assets/js/**/jquery*.js',
      '/assets/js/**/*.js',
      '/assets/js/**/*.coffee',
    ]

    css :application, [
      '/assets/css/**/*.css',
      '/assets/css/**/*.scss',
    ]
  end

  get "/" do
    # slim :welcome
    redirect to '/credit'
  end

  not_found do
    # status 404
    # 'not found'
    redirect to '/credit'
  end

  get "/credit" do
    slim :credit
  end

  post "/credit" do
    credit_params = params['credit']
    @type_id = credit_params.try(:delete, 'type_id')
    @credit = CreditType.find_by_id(@type_id).build(credit_params)
    @errors = @credit.errors
    @result = @credit.payment_result if @errors.empty?
    # slim :result, layout: false
    if request.xhr?
      slim :result, layout: false
    else
      slim :result
    end
  end

end

